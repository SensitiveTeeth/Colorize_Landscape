# Photo Colorization

Project Demo: https://sensitiveteeth.life

<h3>Introduction</h3>

Given a grayscale photograph as input, Colorization is to hallucinate a plausible, though not necessarily correct, color version of the photograph.  
Instead of recovering the photo, if you create an image which “surprises” - you’ve created something artistically unique!

<h3> Approach </h3>

1. <h4>Data Collection </h4>
- We use puppeteer to scrape 2000+ colored landscape photo in the Web

2. <h4>CIE Lab colorspace</h4>
- Training images are converted from the RGB color space to the Lab color space
- Use the L channel as the input to train the network to predict the ab channels
- Combine the predicted channel ab with the input L channel
- Convert the Lab image back to RGB

3. <h4> Convolutional Neural Network </h4>
- Activation function: ReLU, tanh
- Loss function: Mean square error 
- Optimizer:  Adam 
- Number of epochs: 500
- Validation split: 0.15

<h4> Reference </h4>

Colorful Image Colorization (Richard Z, Phillip I., Alexei A., 2016)