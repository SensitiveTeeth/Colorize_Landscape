// https://www.npmjs.com/package/jpeg-js
// npm install jpeg-js

// https://www.npmjs.com/package/jimp
// npm install --save jimp

const fs = require("fs");
const { promisify } = require("util");
const sleep = promisify(setTimeout);

const jpeg = require("jpeg-js");
const Jimp = require("jimp");

const resizeImg = async () => {
  const imgList = [];

  fs.readdirSync(`./raw_data_set/`).forEach((file) => {
    const path = `./raw_data_set/` + file;
    imgList.push(path);
  });

  // console.log(imgList);

  for (index in imgList) {
    if (index >= 0) {
      const imgPath = imgList[index];
      const newImgPath = imgPath.replace(
        "raw_data_set",
        "raw_data_set_resized"
      );

      const jpegData = fs.readFileSync(imgPath);
      const rawImageData = jpeg.decode(jpegData);

      if (rawImageData.width > rawImageData.height) {
        Jimp.read(imgPath, (err, img) => {
          if (err) throw err;
          img
            .resize(Jimp.AUTO, 256) // resize
            .write(newImgPath); // save
        });
      } else if (rawImageData.height > rawImageData.width) {
        Jimp.read(imgPath, (err, img) => {
          if (err) throw err;
          img
            .resize(256, Jimp.AUTO) // resize
            .write(newImgPath); // save
        });
      } else if (rawImageData.height == rawImageData.width) {
        Jimp.read(imgPath, (err, img) => {
          if (err) throw err;
          img
            .resize(256, 256) // resize
            .write(newImgPath); // save
        });
      }
      await sleep(5);
      console.log(newImgPath);
    }
  }
};

const main = async () => {
  await resizeImg();
};

main();
