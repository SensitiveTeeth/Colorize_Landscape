// https://www.npmjs.com/package/image-downloader
// npm install --save image-downloader

const fs = require("fs");
const util = require("util");

const download = require("image-downloader");
const readFile = util.promisify(fs.readFile);

const readJson = async (keyword) => {

  const rawData = await readFile(`${keyword}.json`);
  const data = JSON.parse(rawData);
  let fileNameNum = 0


  if (!fs.existsSync(`./raw_data_set`)) {
    fs.mkdirSync(`./raw_data_set`);
  }


  if (!fs.existsSync(`./raw_data_set/${keyword}`)) {
    fs.mkdirSync(`./raw_data_set/${keyword}`);
  }

  for (let index in data) {
    const url = data[index].url;

    if (index) {
      options = {
        url: url,
        dest: `./raw_data_set/${keyword}/img${keyword}${fileNameNum}.jpeg`,
        // extractFilename: false
      };

      download
        .image(options)
        .then(({ filename }) => {
          console.log("Saved to", filename);
        })
        .catch((err) => console.error(err));

      fileNameNum++;//rename file name

    }
  }

}

readJson("nature");
readJson("national geographic nature");
readJson("landscape photography");

