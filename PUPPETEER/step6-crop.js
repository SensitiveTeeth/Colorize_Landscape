// https://www.npmjs.com/package/jpeg-js
// npm install jpeg-js

// https://www.npmjs.com/package/jimp
// npm install --save jimp

const fs = require("fs");
const { promisify } = require("util");
const sleep = promisify(setTimeout);

const jpeg = require("jpeg-js");
const Jimp = require("jimp");

const resizeImg = async () => {
  const imgList = [];

  fs.readdirSync(`./raw_data_set_resized/`).forEach((file) => {
    const path = `./raw_data_set_resized/` + file;
    imgList.push(path);
  });

  // console.log(imgList);

  for (index in imgList) {
    if (index >= 0) {
      const imgPath = imgList[index];
      const newImgPath = imgPath.replace(
        "raw_data_set_resized",
        "raw_data_set_crop"
      );

      const newimgPathPart = newImgPath.slice(0, -5)

      const jpegData = fs.readFileSync(imgPath);
      const rawImageData = jpeg.decode(jpegData);

      if (rawImageData.width > rawImageData.height) {
        const n = Math.floor(rawImageData.width / 256);
        
        if (n >= 2) {
          Jimp.read(imgPath, (err, img) => {
              if (err) throw err;
              img.crop(0, 0, 256, 256).write(newimgPathPart + '_left.jpeg'); // save
            });
            
            Jimp.read(imgPath, (err, img) => {
                if (err) throw err;
                img.crop(rawImageData.width - 256, 0, 256, 256).write(newimgPathPart + '_right.jpeg'); // save
            });
        }

        const x = Math.floor((rawImageData.width - 256) / 2);
        Jimp.read(imgPath, (err, img) => {
          if (err) throw err;
          img.crop(x, 0, 256, 256).write(newImgPath); // save
        });

      } else if (rawImageData.height > rawImageData.width) {
        const n = Math.floor(rawImageData.height / 256);
        
        if (n >= 2) {
          Jimp.read(imgPath, (err, img) => {
              if (err) throw err;
              img.crop(0, 0, 256, 256).write(newimgPathPart + '_upper.jpeg'); // save
            });
            
            Jimp.read(imgPath, (err, img) => {
                if (err) throw err;
                img.crop(0, rawImageData.height - 256, 256, 256).write(newimgPathPart + '_lower.jpeg'); // save
            });
        }

        const y = Math.floor((rawImageData.height - 256) / 2);
        Jimp.read(imgPath, (err, img) => {
          if (err) throw err;
          img.crop(0, y, 256, 256).write(newImgPath); // save
        });
      }
      await sleep(5);
      console.log(newImgPath);
    }
  }
};

const main = async () => {
  await resizeImg();
};

main();
