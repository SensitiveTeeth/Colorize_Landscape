import sys

dir_path = sys.argv[1]
dir_path2 = sys.argv[2]

import tensorflow as tf
import keras
from tensorflow import keras

model_filepath = 'colorization_model_old'

model = tf.keras.models.load_model(
    model_filepath,
    custom_objects=None,
    compile=False
)

import numpy as np

import scipy as sp
import scipy.ndimage as spi

from skimage.io import imsave,imshow
from skimage.transform import resize
from skimage.color import rgb2lab, lab2rgb, rgb2gray, gray2rgb

import matplotlib.pyplot as plt

# import keras
from keras.preprocessing import image
from keras.preprocessing.image import ImageDataGenerator
from keras.preprocessing.image import array_to_img, img_to_array, load_img

from keras.applications.vgg16 import VGG16
from keras.applications.vgg16 import preprocess_input

DIM = 256

sample_img = []
sample_img.append(sp.misc.imresize(load_img(dir_path), 
                                     (DIM, DIM)))
sample_img = np.array(sample_img, 
                    dtype=float)
sample_img = 1.0/255*sample_img
sample_img = gray2rgb(rgb2gray(sample_img))

sample_img = rgb2lab(sample_img)[:,:,:,0]
sample_img = sample_img.reshape(sample_img.shape+(1,))

# ## load pretrained VGG16
# #Load weights
vgg16 = VGG16(weights='imagenet', include_top=True)
vgg16.graph = tf.get_default_graph()
vgg16.output_shape

# #embedding input
# #Create embedding
def create_vgg_embedding(grayscaled_rgb):
    gs_rgb_resized = []
    for i in grayscaled_rgb:
        i = resize(i, (224, 224, 3), 
                   mode='constant')
        gs_rgb_resized.append(i)
    gs_rgb_resized = np.array(gs_rgb_resized)
    gs_rgb_resized = preprocess_input(gs_rgb_resized)
    with vgg16.graph.as_default():
      embedding = vgg16.predict(gs_rgb_resized)
    return embedding

sample_img_embed = create_vgg_embedding(sample_img)

# # Test model
output_img = model.predict([sample_img, sample_img_embed])
output_img = output_img * 128

# # Output colorizations
for i in range(len(output_img)):
    final_img = np.zeros((DIM,DIM, 3))
    
    # add grayscale channel
    final_img[:,:,0] = sample_img[i][:,:,0]
    
    # add predicted channel
    final_img[:,:,1:] = output_img[i]
    
plt.imsave(dir_path2, lab2rgb(final_img))

# for n, a in enumerate(sys.argv):
#     print('arg {} has value {} endOfArg'.format(n, a))
# print("Load model = " + str(dir_path) + "!")

for n, a in enumerate(sys.argv):
    'arg {} has value {} endOfArg'.format(n, a)
print('{ "success": true }')