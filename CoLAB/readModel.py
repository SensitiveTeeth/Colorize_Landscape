#%%
from flask import Flask,request
import sys
import tensorflow as tf
import os
from keras.models import load_model
from matplotlib import image
from matplotlib import pyplot
import keras
from keras.engine import Layer
from keras.models import Sequential, Model
from keras.layers import Conv2D, Conv3D, UpSampling2D, InputLayer, Conv2DTranspose, Input, Reshape, merge, concatenate
from keras.layers import Activation, Dense, Dropout, Flatten
from keras.layers.normalization import BatchNormalization
from keras.preprocessing import image
from keras.preprocessing.image import ImageDataGenerator, array_to_img, img_to_array, load_img
from keras.callbacks import TensorBoard

from skimage.color import rgb2lab, lab2rgb, rgb2gray, gray2rgb, lab2xyz
from skimage.transform import resize
import numpy as np
from os import listdir
import random

app = Flask(__name__)
#%%
model = load_model('0.15-1000epoch-samson-set2.0')

#%%
#@app.route("/output",methods=['GET'])
#def convent():



file_path = './testInput/'
file_list = list()
for filename in listdir(file_path):
    file_list.append(filename)


test_files = file_list

color_me = []
for imgName in test_files:
  img = img_to_array(load_img(file_path + imgName))
  img = resize(img ,(256,256))
  color_me.append(img)
  
color_me = np.array(color_me, dtype=float)
color_me = rgb2lab(1.0/255*color_me)[:,:,:,0]
color_me = color_me.reshape(color_me.shape+(1,))


output = model.predict(color_me)
output = output * 128

# Output colorizations
for i in range(len(test_files)):
    result = np.zeros((256, 256, 3))
    result[:,:,0] = color_me[i][:,:,0]
    result[:,:,1:] = output[i]
    # imsave("result.png", lab2rgb(result))
    pyplot.imshow(lab2rgb(result))
    filename = test_files[i]
    pyplot.savefig('./testOutput/'+filename)
    pyplot.show()
    
    