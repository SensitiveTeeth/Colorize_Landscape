# BAD-PROJECT

## steps

### Part 1 (LOCAL)

- [x] init TS project (WSP002)

- [x] install knex pg dotenv

```Bash
yarn add knex @types/knex pg @types/pg dotenv
```

- [x] init knex project

```Bash
yarn knex init -x ts
```

- [x] config `.env`

```Text
DB_NAME=c09_bad009_lecture
DB_USERNAME=jason
DB_PASSWORD=jason
```

- [x] create database

```SQL
CREATE DATABASE c09_bad009_lecture;
```

- [x] config `knexfile.ts`

- [x] migrate:make create-init (users, messages)

- [x] config migration

- [x] migrate:latest

yarn knex migrate:latest

- [x] seed:make

- [x] config seed

- [x] seed:run

yarn knex seed:run

- [x] create express template in `main.ts`

- [x] take a rest

- [ ] create `routes.ts`, `controllers` and `services` folders

- [ ] config `MessageController`, `MessageService`, `routes.ts`, `main.ts`

- [ ] manual test + jest test

- [ ] push to gitlab

