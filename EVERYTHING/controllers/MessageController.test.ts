// import { MessageController } from "./MessageController";
// import { MessageService } from "../services/MessageService";
// import Knex from "knex";

// import { Request, Response } from "express";
// jest.mock("express");

// describe("MessageController", () => {
//   let controller: MessageController;
//   let service: MessageService;
//   let resJson: jest.SpyInstance;
//   let req: Request;
//   let res: Response;

//   beforeEach(function () {
//     service = new MessageService({} as Knex);
//     jest
//       .spyOn(service, "getMessages")
//       .mockImplementation(() =>
//         Promise.resolve([{ id: 1, content: "This is CW CHOI's website" }])
//       );

//     controller = new MessageController(service);
//     req = ({
//       body: {},
//       params: {},
//       session: {},
//     } as any) as Request;
//     res = ({
//       json: () => { },
//     } as any) as Response;
//     resJson = jest.spyOn(res, "json");
//   });

//   it("should handle get method correctly", async () => {
//     await controller.get(req, res);
//     expect(service.getMessages).toBeCalledTimes(1);
//     expect(resJson).toBeCalledWith({
//       messages: [{ id: 1, content: "This is CW CHOI's website" }],
//     });
//   });
// });

it("should get all messages", () => {
  const messages = ["first", "second"]
  expect(messages.length).toBe(2)
})
