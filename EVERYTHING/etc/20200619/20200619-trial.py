#%%
import tensorflow as tf
print('TF Version:', tf.__version__)

# %%
import keras
print('Keras Version:', keras.__version__)

# %%
import PIL
print('Pillow Version:', PIL.__version__)

# %%
# # load and show an image with Pillow
# from PIL import Image
# # load the image
# image = Image.open('raw_data_set/image00001.jpeg')
# # summarize some details about the image
# print(image.format)
# print(image.mode)
# print(image.size)
# # show the image
# image.show()

# %%
# # load and display an image with Matplotlib
# from matplotlib import image
# from matplotlib import pyplot
# # load image as pixel array
# data = image.imread('raw_data_set/image00001.jpeg')
# # summarize shape of the pixel array
# print(data.dtype)
# print(data.shape)
# # display the array of pixels as an image
# pyplot.imshow(data)
# pyplot.show()

# %%
# # load all images in a directory
from os import listdir
from matplotlib import image

# # load all images in a directory
# file_list = list()
# for filename in listdir('raw_data_set'):
# 	# load image
# 	img_data = image.imread('raw_data_set/' + filename)
# 	# store loaded image
# 	file_list.append(img_data)
# 	print('> loaded %s %s' % (filename, img_data.shape))

#%%
file_list = list()
for filename in listdir('raw_data_set'):
    file_list.append(filename)

#%%
import keras 
from keras.applications.vgg16 import VGG16
from keras.applications.vgg16 import preprocess_input

vgg16 = VGG16(weights='imagenet', include_top=True)
vgg16.graph = tf.get_default_graph()
vgg16.output_shape

# %%
print(file_list)

# %%
from sklearn.model_selection import train_test_split
train_files,test_files =train_test_split(file_list, 
                                            test_size=0.05,  
                                            random_state=42)
len(train_files),len(test_files)

#%%

print(train_files)

# %%
import keras
from keras.preprocessing import image
from keras.preprocessing.image import ImageDataGenerator
from keras.preprocessing.image import array_to_img, img_to_array, load_img

import scipy as sp
import scipy.ndimage as spi
import numpy as np

def prep_data(file_list=[],
              dir_path=None,
              dim_x=256,
              dim_y=256):
    #Get images
    X = []
    for filename in file_list:
        X.append(img_to_array(
            sp.misc.imresize(
                load_img('raw_data_set/' + filename),
                (dim_x, dim_y))
            )
        )
    X = np.array(X, dtype=np.float64)
    X = 1.0/255*X
    return X

# %%
DIM = 256
X_train = prep_data(file_list=train_files,
                    dir_path='',
                    dim_x=DIM,dim_y=DIM)

#%%
from keras.engine import Layer
from keras.layers import Reshape, merge, concatenate
from keras.layers import Input,Activation, Dense, Dropout, Flatten
from keras.layers import Conv2D, UpSampling2D, InputLayer, Conv2DTranspose 

from keras.layers.core import RepeatVector, Permute
from keras.layers.normalization import BatchNormalization

from keras.callbacks import TensorBoard 

from keras.models import Sequential, Model

emd_input = Input(shape=(1000,))

#Encoder
enc_input = Input(shape=(DIM, DIM, 1,))
enc_output = Conv2D(64, (3,3), 
                        activation='relu', 
                        padding='same', strides=2)(enc_input)
enc_output = Conv2D(128, (3,3), 
                        activation='relu', 
                        padding='same')(enc_output)
enc_output = Conv2D(128, (3,3), 
                        activation='relu', 
                        padding='same', strides=2)(enc_output)
enc_output = Conv2D(256, (3,3), 
                        activation='relu', 
                        padding='same')(enc_output)
enc_output = Conv2D(256, (3,3), 
                        activation='relu', 
                        padding='same', strides=2)(enc_output)
enc_output = Conv2D(512, (3,3), 
                        activation='relu', 
                        padding='same')(enc_output)
enc_output = Conv2D(512, (3,3), 
                        activation='relu', 
                        padding='same')(enc_output)
enc_output = Conv2D(256, (3,3), 
                        activation='relu', 
                        padding='same')(enc_output)

#Fusion
fusion_layer_output = RepeatVector(32*32)(emd_input) 
fusion_layer_output = Reshape(([32,32, 
                          1000]))(fusion_layer_output)
fusion_layer_output = concatenate([enc_output, 
                                   fusion_layer_output], axis=3) 
fusion_layer_output = Conv2D(DIM, (1, 1), 
                       activation='relu', 
                       padding='same')(fusion_layer_output) 

#Decoder
dec_output = Conv2D(128, (3,3), 
                        activation='relu', 
                        padding='same')(fusion_layer_output)
dec_output = UpSampling2D((2, 2))(dec_output)
dec_output = Conv2D(64, (3,3), 
                        activation='relu', 
                        padding='same')(dec_output)
dec_output = UpSampling2D((2, 2))(dec_output)
dec_output = Conv2D(32, (3,3), 
                        activation='relu', 
                        padding='same')(dec_output)
dec_output = Conv2D(16, (3,3), 
                        activation='relu', 
                        padding='same')(dec_output)
dec_output = Conv2D(2, (3, 3), 
                        activation='tanh', 
                        padding='same')(dec_output)
dec_output = UpSampling2D((2, 2))(dec_output)

model = Model(inputs=[enc_input, emd_input], outputs=dec_output)

#%%
#Generate training data
BATCH_SIZE = 32
EPOCH= 1000
STEPS_PER_EPOCH = 1

# %%
import scipy as sp
import scipy.ndimage as spi

from skimage.io import imsave,imshow
from skimage.transform import resize
from skimage.color import rgb2lab, lab2rgb, rgb2gray, gray2rgb

import matplotlib.pyplot as plt

#Create embedding
def create_vgg_embedding(grayscaled_rgb):
    gs_rgb_resized = []
    for i in grayscaled_rgb:
        i = resize(i, (224, 224, 3), 
                   mode='constant')
        gs_rgb_resized.append(i)
    gs_rgb_resized = np.array(gs_rgb_resized)
    gs_rgb_resized = preprocess_input(gs_rgb_resized)
    with vgg16.graph.as_default():
      embedding = vgg16.predict(gs_rgb_resized)
    return embedding

# Image transformer
datagen = ImageDataGenerator(
        shear_range=0.2,
        zoom_range=0.2,
        rotation_range=20,
        horizontal_flip=True)

def colornet_img_generator(X,
                  batch_size=BATCH_SIZE):
    for batch in datagen.flow(X, batch_size=batch_size):
        gs_rgb = gray2rgb(rgb2gray(batch))
        batch_lab = rgb2lab(batch)
        
        batch_l = batch_lab[:,:,:,0]
        batch_l = batch_l.reshape(batch_l.shape+(1,))
        
        batch_ab = batch_lab[:,:,:,1:] / 128
        yield ([batch_l, 
                create_vgg_embedding(gs_rgb)], batch_ab)

# %%
model.compile(optimizer='adam', loss='mse')

# %%
history = model.fit_generator(colornet_img_generator(X_train,
                                                     BATCH_SIZE), 
                              epochs=EPOCH, 
                              steps_per_epoch=STEPS_PER_EPOCH)

# %%
model.summary()

# %%
f, (ax2) = plt.subplots(1, 1, figsize=(25, 5))
t = f.suptitle('ColorNet Performance', fontsize=12)
f.subplots_adjust(top=0.85, wspace=0.3)

epochs = list(range(1,EPOCH+1))

ax2.plot(epochs, history.history['loss'], label='Train Loss')
#ax2.plot(epochs, history.history['val_loss'], label='Validation Loss')
ax2.set_xticks(epochs)
ax2.set_ylabel('Loss Value')
ax2.set_xlabel('Epoch')
ax2.set_title('Loss')
l2 = ax2.legend(loc="best")

plt.tight_layout()
f.autofmt_xdate()
plt.show()

#%%
# import random

# three_test_files = random.choices(test_files, k =3)
# for filename in three_test_files:
#     print(filename)
    
#%%
#Make predictions on validation images
IMG_DIR = 'raw_data_set/'
sample_img = []
for filename in test_files:
    sample_img.append(sp.misc.imresize(load_img(IMG_DIR+filename), 
                                     (DIM, DIM)))
sample_img = np.array(sample_img, 
                    dtype=float)
sample_img = 1.0/255*sample_img
sample_img = gray2rgb(rgb2gray(sample_img))

sample_img = rgb2lab(sample_img)[:,:,:,0]
sample_img = sample_img.reshape(sample_img.shape+(1,))

#embedding input
sample_img_embed = create_vgg_embedding(sample_img)

# %%
# Test model
output_img = model.predict([sample_img, sample_img_embed])
output_img = output_img * 128

#%%
# filenames = test_files
filenames = test_files
# Output colorizations
for i in range(len(output_img)):
    fig = plt.figure(figsize=(8,8))
    final_img = np.zeros((DIM,DIM, 3))
    
    # add grayscale channel
    final_img[:,:,0] = sample_img[i][:,:,0]
    
    # add predicted channel
    final_img[:,:,1:] = output_img[i]
    
    img_obj = load_img(IMG_DIR+filenames[i])
    
    fig.add_subplot(1, 3, 1)
    plt.axis('off')
    
    grayed_img = gray2rgb(
                  rgb2gray(
                      img_to_array(
                          img_obj)/255)
                  )
    plt.imshow(grayed_img)
    plt.title("grayscale")
    
    fig.add_subplot(1, 3, 2)
    plt.axis('off')
    imshow(lab2rgb(final_img))
    plt.title("hallucination")
    
    fig.add_subplot(1, 3, 3)
    plt.imshow(img_obj)
    plt.title("original")
    plt.axis('off')
    plt.show()

#%%
model.save('20200621-model-test-samson')

# %%
