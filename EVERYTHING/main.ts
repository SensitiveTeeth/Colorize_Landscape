// libraries
import express from 'express';
import expressSession from "express-session";
import bodyParser from 'body-parser';
import multer from "multer";

const app = express();

// express-session
app.use(
    expressSession({
        secret: "Colorize Landscape",
        resave: true,
        saveUninitialized: true,
    })
);

// logger
// grant

// db
import Knex from 'knex';
const knexConfig = require('./knexfile');
const knex = Knex(knexConfig[process.env.NODE_ENV || "development"])

// body parser
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// image upload
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, `${__dirname}/public/uploads`);
    },
    filename: function (req, file, cb) {
        cb(null, `${file.fieldname}-${Date.now()}.${file.mimetype.split("/")[1]}`);
    },
});
export const upload = multer({ storage });

export const rootDir = __dirname

// import services controllers routes
import { MessageService } from './services/MessageService';
import { MessageController } from './controllers/MessageController';

const messageService = new MessageService(knex);
export const messageController = new MessageController(messageService);

import { routes } from './routes';
// const API_VERSION = '/api/v1'

app.use(routes);

import path from 'path';
app.use(express.static(path.join(__dirname, 'public')));

// server
const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
    console.log(`[info] listening to Port: ${PORT}`);
});

//python api fetch
// app.post('/uploadAudio', audioUpload.single('audio'), async (req, res) => {
//     if (req.file) {
//         // req.file.filename
//         // console.log(req.file.filename)
//         // node-fetch
//         console.log('Uploading file...');
//         const filename = req.file.filename;
//         console.log(filename);
//         const resp = await fetch('http://localhost:4000', {
//             method: "POST",
//             headers: {
//                 "Content-Type": "application/json"
//             },
//             body: JSON.stringify({filename})
//         });
//         const result = await resp.json();
//         console.log(result);
//         res.json({ 'message': 'test' });
//     } else {
//         console.log('No File Uploaded');
//     }
// });