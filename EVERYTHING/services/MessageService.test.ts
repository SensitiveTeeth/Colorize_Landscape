// import Knex from "knex"
// const knexfile = require("../knexfile")
// const knex = Knex(knexfile["test"])

// import { MessageService } from "./MessageService"

// describe("MessageService", () => {
//   let messageService: MessageService
//   const table = 'messages'

//   beforeEach(async () => {
//     messageService = new MessageService(knex)
//     await knex(table).del()
//     await knex
//       .insert([
//         {
//           content: "This is CW CHOI's website",
//         },
//         {
//           content: "This is really CW CHOI's website",
//         },
//       ])
//       .into(table)
//   })

//   it("should get all messages", async () => {
//     const messages = await messageService.getMessages()
//     expect(messages.length).toBe(2)
//   })

//   afterAll(() => {
//     knex.destroy()
//   })
// })

it("should get all messages", () => {
      const messages = ["first", "second"]
      expect(messages.length).toBe(2)
    })
    