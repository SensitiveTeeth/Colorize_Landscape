const messageObj = document.getElementById('message')
const pageTwoDivObj = document.getElementById("page-2__div")

const clearInfo = () => {
    pageTwoDivObj.style.display = 'none'
    document.getElementById('message').innerHTML = ""
    document.getElementById('black-and-white').innerHTML = ""
    document.getElementById('colorized').innerHTML = ""
    document.getElementById('result__div').style.display = 'none'
}

document.querySelector('#add-image-form')
    .addEventListener('submit', async(event) => {
        event.preventDefault(); // No need to reload the page
        // console.log("clicked")
        clearInfo()

        // upload image 
        const form = event.target;
        const formData = new FormData();
        // formData.append('content', form.content.value);

        formData.append('image', form.image.files[0])

        await fetch('messages', {
                method: "POST",
                body: formData
            })
            .then((res) => {
                const result = res.json()
                return result
            })
            .then((messageID) => {
                document.querySelector('#upload').value = null
                    // if succeed
                    // show message
                messageObj.style.color = 'white'
                messageObj.innerHTML = '<div>UPLOAD</div><div>SUCCEEDED!</div><br><div>COLORIZING!</div><div>PLEASE WAIT!</div>'
                messageObj.style.display = "flex"

                pageTwoDivObj.style.display = 'flex'
                pageTwoDivObj.scrollIntoView({ behavior: "smooth" })

                // load img
                const res = fetch(`/api/v1/messages/${messageID}`);
                return res
            }).then((res) => {
                const message = res.json();
                return message
            }).then((message) => {
                const fileName = message['messages'][0]['content']

                const url = `./uploads/colorized/${fileName}`

                let n = 1
                const checkUrl = (url) => {
                    console.log(`Trial ${n}: Data is processing.`)
                    fetch(url)
                        .then((response) => {
                            return response.status
                        })
                        .then((status) => {
                            if (status !== 404) {
                                console.log(`Trial ${n}: Succeed.`)
                                const greyContent = `<img src='./uploads/grey/${fileName}' class="img-fluid">`
                                const colorizedContent = `<img src='./uploads/colorized/${fileName}' class="img-fluid">`
                                messageObj.style.display = 'none'
                                document.getElementById('black-and-white').innerHTML = greyContent
                                document.getElementById('colorized').innerHTML = colorizedContent
                                document.getElementById('result__div').style.display = 'flex'
                            } else {
                                n = n + 1
                                setTimeout(() => {
                                    checkUrl(url)
                                }, 5000)
                            }
                        })
                }

                checkUrl(url)
            })
    })