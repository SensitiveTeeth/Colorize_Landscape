import express from 'express';
import {messageController} from './main';
import { upload } from "./main";

export const routes = express.Router();

routes.get('/messages/:messageID', messageController.get);
routes.post("/messages", upload.single("image"), messageController.add);
// routes.post("/messages", messageController.add);

