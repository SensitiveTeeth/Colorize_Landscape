import * as Knex from "knex";

export async function seed(knex: Knex): Promise<any> {
  await knex("messages").del();
  await knex("messages").insert([
    {
      content: "This is the testing webpage of the BAD project.",
    },
    {
      content: "Welcome!",
    },
  ]);
}
