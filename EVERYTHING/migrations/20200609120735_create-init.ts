import * as Knex from "knex";


export async function up(knex: Knex): Promise<any> {
	await knex.schema.createTable('users', table => {
		table.increments();
		table.string('username').notNullable().unique();
		table.string('password').notNullable();
		table.timestamps(false, true);
	});
	await knex.schema.createTable('messages', table => {
		table.increments();
		table.text('content').notNullable();
		table.timestamps(false, true);
	});
}


export async function down(knex: Knex): Promise<any> {
	await knex.schema.dropTable('messages');
	await knex.schema.dropTable('users');
}

