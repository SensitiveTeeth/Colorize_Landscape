// https://www.npmjs.com/package/images-scraper
// npm install images-scraper

const Scraper = require("images-scraper");
const fs = require("fs");

const saveLinksToJson = async (keyword) => {
  const google = new Scraper({
    puppeteer: {
      headless: true,
    },
  });

  const results = await google.scrape(keyword, 550);

  console.log("results", results);

  // stringify JSON Object
  var jsonContent = JSON.stringify(results);
  //   console.log(jsonContent);

  fs.writeFileSync(`${keyword}.json`, jsonContent, "utf8", function (err) {
    if (err) {
      console.log("An error occured while writing JSON Object to File.");
      return console.log(err);
    }

    console.log("JSON file has been saved.");
  });
};

const main = async () => {
  await saveLinksToJson("nature");
  await saveLinksToJson("national geographic nature");
  await saveLinksToJson("landscape photography");
}

main()