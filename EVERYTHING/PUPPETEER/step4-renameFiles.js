const fs = require("fs");
const files = fs.readdirSync(`./raw_data_set/`)

function pad(number, length) {
    var str = '' + number;
    while (str.length < length) {
        str = '0' + str;
    }
    return str;
}

for (index in files) {
    const number = pad(+index + 1, 5)
    fs.renameSync(`./raw_data_set/${files[index]}`, `./raw_data_set/image${number}.jpeg`);
}