// https://www.npmjs.com/package/jimp
// npm install --save jimp

// https://www.npmjs.com/package/image-type
// npm install image-type
// npm install read-chunk 

// https://github.com/lovell/sharp/
// npm install sharp

// for files in raw_data_set2 (201~ ? photo only)

const Jimp = require("jimp");
const fs = require("fs");
const { promisify } = require("util");
const sleep = promisify(setTimeout);

const readChunk = require('read-chunk');
const imageType = require('image-type');

const sharp = require('sharp')

const resizeImg = async (keyword) => {
  const imgList = [];

  fs.readdirSync(`./raw_data_set2/${keyword}/`).forEach((file) => {
    const path = `./raw_data_set2/${keyword}/` + file;
    imgList.push(path);
  });

  // console.log(imgList);

  for (index in imgList) {
    const imgPath = imgList[index]

    if (index) {
      const newPngImgPath = imgPath.replace("jpeg", "png");
      const newImgPath = imgPath.replace("raw_data_set2", "data-set2");
  
      const buffer = readChunk.sync(imgPath, 0, 12);
      const imageInfo = imageType(buffer);
  
      if (imageInfo && imageInfo.mime !== "image/jpeg") {
        // console.log(imgPath)
        // console.log(imageInfo)
  
        sharp(imgPath)
          // .rotate()
          .toFile(newPngImgPath, (err, info) => {
            // console.log(info)
            info
          })
  
        sharp(newPngImgPath)
          // .rotate()
          .toFile(imgPath, (err, info) => {
            // console.log(info)
            info
          })
      }
  
      if (imageInfo && imageInfo.mime == "image/jpeg") {
         Jimp.read(imgPath, (err, img) => {
          if (err) throw err;
          img
              .resize(128, 128) // resize
            // .quality(50) // set JPEG quality
            .greyscale() // set greyscale
            .write(newImgPath); // save
        });
        await sleep(1);
        console.log(newImgPath)
        
          const mu = process.memoryUsage();
          console.log(mu)
        
      }
    }
  }
};

const main = async () => {
  await resizeImg("nature");
  // await resizeImg("national geographic nature");
  // await resizeImg("landscape photography");
};

main();
