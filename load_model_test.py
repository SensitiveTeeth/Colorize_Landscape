print('Python started')

# import sys

dir_path = './test.jpg'
dir_path2 = './test_c.jpg'

import tensorflow as tf
# import keras
from tensorflow import keras

model_filepath = 'colorization_model'

model = tf.keras.models.load_model(
    model_filepath,
    custom_objects=None,
    compile=False
)

import keras
# from keras.engine import Layer
from keras.models import Sequential, Model
# from keras.layers import Conv2D, Conv3D, UpSampling2D, InputLayer, Conv2DTranspose, Input, Reshape, merge, concatenate
# from keras.layers import Activation, Dense, Dropout, Flatten
# from keras.layers.normalization import BatchNormalization
from keras.preprocessing import image
from keras.preprocessing.image import ImageDataGenerator, array_to_img, img_to_array, load_img
# from keras.callbacks import TensorBoard

from skimage.color import rgb2lab, lab2rgb, rgb2gray, gray2rgb, lab2xyz
from skimage.transform import resize
import numpy as np

color_me = []
img = img_to_array(load_img(dir_path))
img = resize(img ,(256,256))
color_me.append(img)
  
color_me = np.array(color_me, dtype=float)
color_me = rgb2lab(1.0/255*color_me)[:,:,:,0]
color_me = color_me.reshape(color_me.shape+(1,))
# color_me.shape

output = model.predict(color_me)
output = output * 128

# # Output colorizations
from matplotlib import image
from matplotlib import pyplot
import matplotlib.pyplot as plt

for i in range(1):
    result = np.zeros((256, 256, 3))
    result[:,:,0] = color_me[i][:,:,0]
    result[:,:,1:] = output[i]
    plt.imsave(dir_path2, lab2rgb(result))

# for n, a in enumerate(sys.argv):
#     print('arg {} has value {} endOfArg'.format(n, a))
    
print('Python ended')