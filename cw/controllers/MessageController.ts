import { MessageService } from '../services/MessageService';
import { Request, Response } from 'express';

import { rootDir } from '../main'

const Jimp = require("jimp");
const path = require('path')
const fs = require("fs");
// const jpeg = require("jpeg-js");

const { PythonShell } = require('python-shell');

const runPython = (keyword1: string, keyword2: string) => {
	console.log('Python shell started')
	let options = {
		mode: 'text',
		pythonPath: '/usr/bin/python3',
		pythonOptions: ['-u'], // get print results in real-time
		scriptPath: path.join(rootDir),
		args: [keyword1, keyword2]
	};

	PythonShell.run('load_model.py', options, function (err: Error, results: string) {
		if (err) throw err;
		// results is an array consisting of messages collected during execution
		console.log(results);
	});
	console.log('Python shell ended')
}

const greyizeImg = async (fileName: string) => {
	console.log('Start jimping')
	const imgPath = path.join(rootDir, 'public', 'uploads', fileName)
	const imgGreyPath = path.join(rootDir, 'public', 'uploads', 'grey', fileName)

	Jimp.read(imgPath, (err: Error, img: any) => {
		if (err) throw err;
		img.greyscale()
			.resize(256, 256) // resize
			.write(imgGreyPath); // save
	});

	console.log('Finish jimping')
	return
};

export class MessageController {

	constructor(private service: MessageService) { }

	get = async (req: Request, res: Response) => {
		try {
			const messageID = parseInt(req.params.messageID);
			if (isNaN(messageID)) {
				// res.status(400).json({ msg: "id is not a number!" });
				res.status(400).json({ msg: messageID });
				return;
			}
			const messages = await this.service.getMessages(messageID);
			res.json({ messages });
		} catch (err) {
			console.error(err.message);
			res.status(500).json({ message: 'internal server error' });
		}
		return
	}

	add = async (req: Request, res: Response) => {
		try {
			// req.file === undefined?
			if (typeof req.file === "undefined") {
				res.status(400).json({ message: "Invalid input" });
				return;
			}
			//   const content = req.body.content;
			const content = req.file.filename

			// const imgGreyPath = await path.join(rootDir, 'public', 'uploads', 'grey', content)
			// const imgColorizedPath = await path.join(rootDir, 'public', 'uploads', 'colorized', content)
			const imgColorizedDirPath = await path.join(rootDir, 'public', 'uploads', 'colorized')

			const imgGreyPath = await path.join('public', 'uploads', 'grey', content)
			const imgColorizedPath = await path.join('public', 'uploads', 'colorized', content)

			// greyize img
			console.log('Jimp function')
			greyizeImg(content)

			if (!fs.existsSync(imgColorizedDirPath)) {
				fs.mkdirSync(imgColorizedDirPath);
			}

			
			console.log('Python shell function')
			runPython(imgGreyPath, imgColorizedPath)

			const messageID = await this.service.addMessages(content)
			console.log(`messageID: ${messageID}`)
			res.json(messageID);
		} catch (e) {
			console.error(e.message);
			res.status(500).json({ message: "Internal server error" });
			return
		}
	};
}