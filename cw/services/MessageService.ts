import Knex from 'knex';
import {IMessage} from './models'

export class MessageService {

	constructor(private knex: Knex) {}

	getMessages = async (messageID: number) => await this.knex<IMessage>('messages').where({id: messageID});

	addMessages = async (content: string) => {
		const [messageID] = await this.knex('messages').insert({
		  content
		}).returning('id');
		return messageID as number;
	  }
}