import * as Knex from "knex";

export async function seed(knex: Knex): Promise<any> {
  await knex("messages").del();
  await knex("messages").insert([
    {
      content: "This is CW CHOI's first webpage.",
    },
    {
      content: "Welcome!",
    },
  ]);
}
