// libraries
import express from 'express';
import expressSession from "express-session";
import bodyParser from 'body-parser';
import multer from "multer";

const fs = require("fs");
const app = express();

// body parser
app.use(bodyParser.urlencoded({ extended: true, limit: '5MB' }));
app.use(bodyParser.json({ limit: '5MB' }));
// express-session
app.use(
    expressSession({
        secret: "Colorize Landscape",
        resave: true,
        saveUninitialized: true,
    })
);

// logger
// grant

// db
import Knex from 'knex';
const knexConfig = require('./knexfile');
const knex = Knex(knexConfig[process.env.NODE_ENV || "development"])




// image upload
// check folder
if (!fs.existsSync(`${__dirname}/public/uploads`)) {
    fs.mkdirSync(`${__dirname}/public/uploads`);
}

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, `${__dirname}/public/uploads`);
    },
    filename: function (req, file, cb) {
        cb(null, `${file.fieldname}-${Date.now()}.${file.mimetype.split("/")[1]}`);
    },
});
export const upload = multer({ storage });

export const rootDir = __dirname

// import services controllers routes
import { MessageService } from './services/MessageService';
import { MessageController } from './controllers/MessageController';

const messageService = new MessageService(knex);
export const messageController = new MessageController(messageService);

import { routes } from './routes';
const API_VERSION = '/api/v1'

app.use(API_VERSION, routes);

import path from 'path';
app.use(express.static(path.join(__dirname, 'public')));

// server
export const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
    console.log(`[info] listening to Port: ${PORT}`);
});
