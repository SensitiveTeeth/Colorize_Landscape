1
command 
python3 -m pip freeze > requirements.txt

2
img link
https://drive.google.com/file/d/18kgnLgRQuI2bikV6dUay4iw_zWIc-g-R/view

folder name => set to raw_data_set

##---installation---##

pip installtensorflow tensorflow-gpu 

pip install --upgrade tensorflow-gpu

docker pull tensorflow/tensorflow

docker run -it -p
    8888:8888
    tensorflow/tensorflow

conda install numpy scipy mkl <nose> <sphinx> <pydot-ng>

conda install theano pygpu

sudo apt-get install openmpi-bin

pip install cntk-gpu

conda install pytorch torchvision -c pytorch

pip3 install torch torchvision

pip install torch torchvision

#---checking---#
from __future__ import print_function
import torch
x = torch.rand(5, 3)
print(x)
